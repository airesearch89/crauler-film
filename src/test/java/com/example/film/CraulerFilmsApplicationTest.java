package com.example.film;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.example.film.model.Film;
import com.example.film.repository.FilmRepository;
import org.apache.commons.io.FilenameUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest()
@ComponentScan(basePackages = { "com.example" })
public class CraulerFilmsApplicationTest {

    private WebDriver webDriver;

    @Autowired
    private FilmRepository filmRepository;
    private ChromeOptions chromeOptions;
    private WebDriver tempWebDriver;

    public WebDriver createWebDriver() {
        return new ChromeDriver(chromeOptions);
    }

    @Bean
    @Primary
    public Logger logger() {
        return LoggerFactory.getLogger("CraulerFilmsApplicationTest");
    }

    public void extractFilmDataFromPage() {
        List<WebElement> filmsWebElements = webDriver.findElements(By.className("_NO_HIGHLIGHT_"));
        logger().info("Find: " + filmsWebElements.size());
        for(WebElement tweetWebElement : filmsWebElements) {
            saveFilm(tweetWebElement);
        }

    }

    private void saveFilm(WebElement webElementTweet) {
        Film film = new Film();
        String href = webElementTweet.findElement(By.className("name")).findElement(By.cssSelector("a"))
                .getAttribute("href");
        if(filmRepository.findFilmByHref(href) != null) {
            return;
        }
        film.setHref(href);
        try {

            tempWebDriver.navigate().to(href);
            TimeUnit.SECONDS.sleep(2);

            List<WebElement> webElementList = tempWebDriver.findElements(By.cssSelector("tr"));
            film.setName(tempWebDriver.findElement(By.className("moviename-big")).getText());
            logger().info(film.getName());
            film.setYear(webElementList.get(0).getText().replaceAll("\\D+", "").substring(0, 4));
            film.setCountry(webElementList.get(1).getText());
            film.setProducer(webElementList.get(4).getText());
            film.setDescription(tempWebDriver.findElement(By.className("brand_words")).getText());
            film.setActors(tempWebDriver.findElements(By.id("actorList")).get(0).getText().replaceAll(" ", "_")
                    .replaceAll("\n", " "));
            String stringRating = tempWebDriver.findElement(By.className("rating_ball")).getText().substring(0, 1);
            Integer rating = new Integer(stringRating);
            film.setRating(rating.toString());

        }
        catch(Exception e) {
            logger().error("header page", e);
        }
        try {
            filmRepository.save(film);
        }
        catch(DataIntegrityViolationException e) {
            logger().warn("duplicate film");
        }
    }

    @Test
    public void runContextTest() {
        try {
            webDriver = createWebDriver();
            tempWebDriver = createWebDriver();
            for(int i = 332; i < 500; i++) {
                try {
                    webDriver.navigate()
                            .to("https://www.kinopoisk.ru/top/navigator/m_act%5Brating%5D/1%3A/order/rating/page/"
                                    + Integer
                                    .toString(i) + "/#results");
                    logger().info("number page:" + Integer.toString(i));
                    TimeUnit.SECONDS.sleep(2);
                    extractFilmDataFromPage();
                }
                catch(Exception e) {
                    logger().error("page " + Integer.toString(i), e);
                }
            }
        }
        finally {
            tempWebDriver.quit();
            webDriver.quit();
        }
    }

    @Before
    public void initChromeOptions() {
        try {
            String chromeAbsolutePath = FilenameUtils
                    .separatorsToSystem(new File("./").getCanonicalPath() + File.separator
                            + "src\\test\\resources\\chrome\\chromedriver.exe");
            System.setProperty("webdriver.chrome.driver", chromeAbsolutePath);
            chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--start-maximized");
        }
        catch(IOException e) {
            throw new RuntimeException("Can not find chromedriver.exe file");
        }
    }
}
