package com.example.film.service;

import com.example.film.model.Film;
import com.example.film.repository.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class FilmService {

    private FilmRepository filmRepository;

    @Autowired
    public void setFilmRepository(FilmRepository filmRepository) {
        this.filmRepository = filmRepository;
    }

    public Page<Film> getUnratedMovies(int page) {
        return filmRepository.findFilmByUserRatingIsNull(PageRequest.of(page - 1, 10));
    }

    public Page<Film> getRecommendedMovies(int page) {
        return filmRepository.findFilm(PageRequest.of(page - 1, 10, Sort.by(
                new Sort.Order(Sort.Direction.DESC, "answerns")
        )));
    }

    public void setRating(Integer index, Integer rating) {
        Film film = filmRepository.findFilmById(Long.valueOf(index));
        film.setUserRating(Integer.toString(rating));
        filmRepository.save(film);
    }

}
