package com.example.film.repository;

import com.example.film.model.Film;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmRepository extends JpaRepository<Film, Long> {

    Film findFilmByHref(String href);

    Film findFilmById(Long id);

    Page<Film> findFilmByUserRatingIsNull(Pageable pageable);

    @Query("SELECT film FROM Film film WHERE film.answerns is not null and  film.answerns>0 ")
    Page<Film> findFilm(Pageable pageable);

}
