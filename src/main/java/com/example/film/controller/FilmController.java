package com.example.film.controller;

import com.example.film.model.Film;
import com.example.film.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FilmController {

    private FilmService filmService;

    @GetMapping("/unrated-movies/{numberPage}")
    public Page<Film> getUnratedMovies(@PathVariable("numberPage") Long numberPage) {
        return filmService.getUnratedMovies(numberPage.intValue());
    }

    @GetMapping("/recommended-movies/{numberPage}")
    public Page<Film> getRecommendedMovies(@PathVariable("numberPage") Long numberPage) {
        return filmService.getRecommendedMovies(numberPage.intValue());
    }

    @PostMapping("/rating/{id}")
    public void rating(@PathVariable(value = "id") Integer id, @RequestBody Integer rating) {
        filmService.setRating(id, rating);
    }

    @Autowired
    public void setFilmService(FilmService filmService) {
        this.filmService = filmService;
    }
}
