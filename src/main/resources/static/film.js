$(document).ready(function () {
    $('#checkBox').change(function () {
        updateFilmList();
    });

    function updateFilmList(newPage) {
        if (typeof newPage === 'undefined') {
            newPage = 1;
        } else {
            if (newPage === -1) {
                newPage = localStorage.getItem("page");
            }
        }
        localStorage.setItem("page", newPage);
        var url;
        if ($('#checkBox').is(':checked')) {
            url = 'http://localhost:8089/recommended-movies/' + newPage
        } else {
            url = 'http://localhost:8089/unrated-movies/' + newPage
        }
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'GET',
            success: function (data) {
                var trHTML = '';
                $('#tbody-view-visitor').text(trHTML);

                data.content.forEach(function (item) {
                        var key = item.href.split('-');
                        key = key[key.length - 1].substring(0, key[key.length - 1].length - 1);
                        trHTML += '<tr id="tr' + item.id + '">' +
                            '<td><a href="' + item.href + '"><img style="width: 150px; height: 250px;" src="https://st.kp.yandex.net/images/film_big/' + key + '.jpg"/></a></td>' +
                            '<td>' + item.name + '</td>' +
                            '<td>' + item.description + '</td>' +
                            '<td>' + item.rating + '</td>' +
                            '<td>' + item.country + '</td>' +
                            '<td>' + item.producer + '</td>' +
                            '<td>' + item.actors + '</td>' +
                            '<td>' + item.year + '</td>' +
                            '<td ><div class="raty" style="width: 100px;" id="' + item.id + '"></div></td></tr>';

                    }
                );

                $('#tbody-view-visitor').append(trHTML);

                $('.raty').raty({
                    number: 10,
                    target: '#target',
                    targetKeep: true,
                    targetType: 'number',
                    click: function (score, evt) {
                        $.ajax({
                            url: 'http://localhost:8089/rating/' + this.id,
                            method: "POST",
                            data: JSON.stringify(score),
                            contentType: "application/json"
                        })
                    }
                });


                var totalPages = data.totalPages;
                if (totalPages === 0) {
                    totalPages = 1;
                }
                var number = data.number + 1;

                $('#count-find-visitor').text("Найдено:" + data.totalElements);
                $('#pagination').bootpag({
                    total: totalPages,
                    startPage: number,
                    maxVisible: 10,
                    leaps: true,
                    firstLastUse: true,
                    first: '←',
                    last: '→',
                    wrapClass: 'pagination',
                    activeClass: 'active',
                    disabledClass: 'disabled',
                    nextClass: 'next',
                    prevClass: 'prev',
                    lastClass: 'last',
                    firstClass: 'first'
                })
            },
            error: function (x, e) {
                console.log(e);
            }

        });


    }

    $('#pagination').bootpag({
        total: 1,
        startPage: 1,
        maxVisible: 10,
        prev: 'Назад',
        next: 'Далее',
    }).on("page", function (event, page) {
        console.info(page);
        $(updateFilmList(page));
        event.preventDefault();
        $('body,html').animate({scrollTop: 0}, 400);
    })
    updateFilmList();
});

